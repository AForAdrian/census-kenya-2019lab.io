function make_map(cityname, dom_id){
  achart = echarts.init(document.getElementById(dom_id));
  var option =  {
    "title": [
      {
	"textStyle": {
	  "color": "#000",
	  "fontSize": 18
	},
	"subtext": "",
	"text": cityname,
	"top": "auto",
	"subtextStyle": {
	  "color": "#aaa",
	  "fontSize": 12
	},
	"left": "auto"
      }
    ],
    "legend": [
      {
	"selectedMode": "multiple",
	"top": "top",
	"orient": "horizontal",
	"data": [
	  ""
	],
	"left": "center",
	"show": true
      }
    ],
            visualMap: {
            left: 'right',
            min:  0,
            max: 25,
            inRange: {
                color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026']
            },
            text: ['High', 'Low'],          
            calculable: true
        },

    "backgroundColor": "#fff",
    	  tooltip: {
		  trigger: 'item',
		  formatter: function (params) {
			  var value = (params.value + '').split('.');
			  value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
			  return params.seriesName + '<br/>' + params.name + ' : ' + value;
		  }
	  },
	  toolbox: {
		  show: true,
		  orient: 'vertical',
		  left: 'right',
		  top: 'center',
        feature: {
            dataView: {readOnly: true},
            restore: {},
            saveAsImage: {}
        }
    },
    "series": [
      {
	"mapType": cityname,
	"data": [
           {name: "Mombasa", value: 5.5},
           {name: "Kwale", value: 6.2},
           {name: "Kilifi", value: 8.1},
           {name: "Tana River", value: 3.7},
           {name: "Lamu", value: 8.6},
           {name: "Taita Taveta", value: 6.7},
           {name: "Garissa", value: 6.0},
           {name: "Wajir", value: 2.4},
           {name: "Mandera", value: 3.2},
           {name: "Marsabit", value: 3.0},
           {name: "Isiolo", value: 7.5},
           {name: "Meru", value: 3.5},
           {name: "Tharaka-Nithi", value: 3.5},
           {name: "Embu", value: 5.6},
           {name: "Kitui", value: 2.7},
           {name: "Machakos", value: 9.4},
           {name: "Makueni", value: 2.5},
           {name: "Nyandarua", value: 2.2},
           {name: "Nyeri", value: 7.2},
           {name: "Kirinyaga", value: 5.5},
           {name: "Murang`a", value: 4.7},
           {name: "Kiambu", value: 19.6},
           {name: "Turkana", value: 1.9},
           {name: "West Pokot", value: 0.6},
           {name: "Samburu", value: 1.8},
           {name: "Trans Nzoia", value: 3.7},
           {name: "Uasin Gishu", value: 8.3},
           {name: "Elegeyo-Marakwet", value: 1.1},
           {name: "Nandi", value: 2.4},
           {name: "Baringo", value: 1.9},
           {name: "Laikipia", value: 5.2},
           {name: "Nakuru", value: 8.9},
           {name: "Narok", value: 1.9},
           {name: "Kajiado", value: 15.8},
           {name: "Kericho", value: 3.0},
           {name: "Bomet", value: 1.4},
           {name: "Kakamega", value: 3.3},
           {name: "Vihiga", value: 3.6},
           {name: "Bungoma", value: 2.8},
           {name: "Busia", value: 3.7},
           {name: "Siaya", value: 4.0},
           {name: "Kisumu", value: 11.6},
           {name: "Homa Bay", value: 3.3},
           {name: "Migori", value: 3.6},
           {name: "Kisii", value: 3.1},
           {name: "Nyamira", value: 2.0},
           {name: "Nairobi", value: 23.5}
    ],
	"name": "Kenya Proportion of Households with Refrigerators (2019)",
	"type": "map",
	"roam": true,
    emphasis: {
				  label: {
					  show:true
				  }
			  }
      },
      
    ]
  };
  achart.setOption(option);
}

