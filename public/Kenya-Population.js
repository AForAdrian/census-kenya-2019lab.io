function make_map(cityname, dom_id){
  achart = echarts.init(document.getElementById(dom_id));
  var option =  {
    "title": [
      {
	"textStyle": {
	  "color": "#000",
	  "fontSize": 18
	},
	"subtext": "",
	"text": cityname,
	"top": "auto",
	"subtextStyle": {
	  "color": "#aaa",
	  "fontSize": 12
	},
	"left": "auto"
      }
    ],
    "legend": [
      {
	"selectedMode": "multiple",
	"top": "top",
	"orient": "horizontal",
	"data": [
	  ""
	],
	"left": "center",
	"show": true
      }
    ],
            visualMap: {
            left: 'right',
            min:  100000,
            max: 5000000,
            inRange: {
                color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026']
            },
            text: ['High', 'Low'],          
            calculable: true
        },

    "backgroundColor": "#fff",
    	  tooltip: {
		  trigger: 'item',
		  formatter: function (params) {
			  var value = (params.value + '').split('.');
			  value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
			  return params.seriesName + '<br/>' + params.name + ' : ' + value;
		  }
	  },
	  toolbox: {
		  show: true,
		  orient: 'vertical',
		  left: 'right',
		  top: 'center',
        feature: {
            dataView: {readOnly: true},
            restore: {},
            saveAsImage: {}
        }
    },
    "series": [
      {
	"mapType": cityname,
	"data": [
           {name: "Mombasa", value: 1208333},
           {name: "Kwale", value: 866820},
           {name: "Kilifi", value: 1453787},
           {name: "Tana River", value: 315943},
           {name: "Lamu", value: 143920},
           {name: "Taita Taveta", value: 340671},
           {name: "Garissa", value: 841353},
           {name: "Wajir", value: 781263},
           {name: "Mandera", value: 867457},
           {name: "Marsabit", value: 459785},
           {name: "Isiolo", value: 268002},
           {name: "Meru", value: 1545714},
           {name: "Tharaka-Nithi", value: 393177},
           {name: "Embu", value: 608599},
           {name: "Kitui", value: 1136187},
           {name: "Machakos", value: 1421932},
           {name: "Makueni", value: 987653},
           {name: "Nyandarua", value: 638289},
           {name: "Nyeri", value: 759164},
           {name: "Kirinyaga", value: 610411},
           {name: "Murang`a", value: 1056640},
           {name: "Kiambu", value: 2417735},
           {name: "Turkana", value: 926976},
           {name: "West Pokot", value: 621241},
           {name: "Samburu", value: 310327},
           {name: "Trans Nzoia", value:	990341},
           {name: "Uasin Gishu", value:	1163186},
           {name: "Elegeyo-Marakwet", value: 454480},
           {name: "Nandi", value: 885711},
           {name: "Baringo", value: 666763},
           {name: "Laikipia", value: 518560},
           {name: "Nakuru", value: 2162202},
           {name: "Narok", value: 1157873},
           {name: "Kajiado", value: 1117840},
           {name: "Kericho", value: 901777},
           {name: "Bomet", value: 875689},
           {name: "Kakamega", value: 1867579},
           {name: "Vihiga", value: 590013},
           {name: "Bungoma", value: 1670570},
           {name: "Busia", value: 893681},
           {name: "Siaya", value: 993183},
           {name: "Kisumu", value: 1155574},
           {name: "Homa Bay", value: 1131950},
           {name: "Migori", value: 1116436},
           {name: "Kisii", value: 1266860},
           {name: "Nyamira", value: 605576},
           {name: "Nairobi", value: 4397073}
    ],
	"name": "Kenya Population (2019)",
	"type": "map",
	"roam": true,
    emphasis: {
				  label: {
					  show:true
				  }
			  }
      },
      
    ]
  };
  achart.setOption(option);
}
